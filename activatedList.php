<?php
include_once './header.php';
include_once './navigation.php';
include_once './vendor/autoload.php';
use App\MDSU\Users\Users;

$user = new Users();
$allUsers = $user->active();
?>
<html>
    <head>
        <title>My Final Project</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        
  </head>
    <body>
            
        
        <div class="main_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 col col-md-offset-3">
                        <table class="table table-striped table-hover">
                  <tr class="info">
                    <th>SL</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allUsers) && !empty($allUsers)) {
                    $serial = 0;
                    foreach ($allUsers as $oneUser) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $oneUser['username']; ?></td>
                            <td><?php echo $oneUser['email']; ?></td>
                            <td>
                                <a href="profile.php?id=<?php echo $oneUser['id'] ?>">Show Details</a> | 
                                <a href="delete.php?id=<?php echo $oneUser['id'] ?>">Delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
    <?php echo "Not Available" ?>

                        </td>
                    </tr>
<?php }
?>

            </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>