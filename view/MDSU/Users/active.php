<?php
include_once '../../../vendor/autoload.php';
use App\MDSU\Users\Users;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$activate = new Users();
$activate ->prepare($_GET);
$activate ->updateActive();
