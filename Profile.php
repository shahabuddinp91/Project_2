<?php
include_once './vendor/autoload.php';
use App\MDSU\Users\Users;
include './header.php';
include './navigation.php';

//session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = $_GET['id'];
}else{
$id = $_SESSION['id'];
}
$user = new Users();
$oneUser = $user->show($id);
?>
<html>
    <head>
        <title>My Final Project</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        
  </head>
    <body>
            
        
        <div class="main_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 col col-md-offset-3">
                        <table class="table table-striped table-hover">
                  <tr> <td>ID </td> <td><?php echo $oneUser['id'] ?></td></tr>
                    <tr><td>User Name </td> <td><?php
                        if (isset($oneUser['username']) && !empty($oneUser['username'])) {
                            echo $oneUser['username'];
                        } else {
                            echo "No data available";
                        }
                        ?>

                    </td>
                    </tr>
                    <tr><td>Created At </td> <td><?php echo $oneUser['created_at'] ?></td></tr>
                    <tr><td>Updated At </td> <td><?php echo $oneUser['modified_at'] ?></td></tr>
              </table>
          </div>
      </div>
<!--        <div class="table-responsive col-md-8">
        <table class="table border-collapsecenter-block">
            <tr> <td>ID </td> <td><?php echo $oneUser['id'] ?></td></tr>
                    <br><tr><td>User Name </td> <td><?php
                        if (isset($oneUser['username']) && !empty($oneUser['username'])) {
                            echo $oneUser['username'];
                        } else {
                            echo "No data available";
                        }
                        ?>

                    </td>
                    </tr><br>
                    <tr><td>Created At </td> <td><?php echo $oneUser['created_at'] ?></td></tr><br>
                    <tr><td>Updated At </td> <td><?php echo $oneUser['modified_at'] ?></td></tr><br>
                    <tr><td></td><td></td></tr>
            </table>
            </div>-->
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>