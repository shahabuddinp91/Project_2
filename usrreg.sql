-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2016 at 08:58 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `usrreg`
--

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `personal_phone` int(11) DEFAULT NULL,
  `home_phone` int(11) DEFAULT NULL,
  `office_phone` int(11) DEFAULT NULL,
  `current_address` varchar(255) DEFAULT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `modified_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_group_id`, `unique_id`, `username`, `password`, `email`, `modified_at`, `created_at`, `deleted_at`, `is_active`) VALUES
(4, 0, '', 'admin', 'admin', 'admin@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(5, 0, '', 'a', 'a', 'abc@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(7, 0, '', 'shahabuddin', '123', 'shahabuddinp91@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(8, 0, '', 'alamgir', 'alamgir', 'alamgir@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(9, 0, '', 'karim', 'karim', 'karim@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(10, 0, '', 'rahim', 'rahim', 'rahim@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(11, 0, '', 'abul', 'abul', 'abul@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(12, 0, '', 'rony', 'rony', 'rony@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(13, 0, '', 'ab', 'ab', 'ab@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(14, 0, '', 'c', 'c', 'c@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(18, 0, '', 'd', 'd', 'd@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `password` (`password`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
