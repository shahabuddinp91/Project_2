<?php
include_once './vendor/autoload.php';
use App\MDSU\Users\Users;
include './header.php';
include './navigation.php';

//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$store = new Users();
$allUsers = $store->index();
?>
<html>
    <head>
        <title>My Final Project</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        
  </head>
    <body>
            
        
        <div class="main_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 col col-md-offset-3">
                        <table class="table table-striped table-hover">
                  <tr class="info">
                    <th>SL</th>
                    <th>User Name</th>
                    <th>Email</th>
                    
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allUsers) && !empty($allUsers)) {
                    $serial = 0;
                    foreach ($allUsers as $oneUser) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $oneUser['username']; ?></td>
                            <td><?php echo $oneUser['email']; ?></td>
                            <td>
                                <a href="profile.php?id=<?php echo $oneUser['id'] ?>">Show Details</a> | 
                                <a href="delete.php?id=<?php echo $oneUser['id'] ?>">Delete</a> | 
                                <a href="view/MDSU/Users/active.php?id=<?php echo $oneUser['id'] ?>">Activate</a> 
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
    <?php echo "Not Available" ?>

                        </td>
                    </tr>
<?php }
?>

            </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>